<?php

require_once("animal.php");
require_once("ape.php");
require_once("frog.php");

$sheep = new Animal("shaun");
echo "Name : " . $sheep->name ."<br>"; // "shaun"
echo "Legs : " . $sheep->legs ."<br>"; // 4
echo "cold blooded: " . $sheep->cold_blooded ."<br>"; // "no"

echo "<br>";

$ape = new Ape("Kera Sakti");
echo "Name : " . $ape->name ."<br>";
echo "Legs : " . $ape->legs ."<br>"; 
echo "cold blooded: " . $ape->cold_blooded ."<br>";
echo "Yell: ";
$sungokong = new Ape("kera sakti");

echo $sungokong->yell() . "<br>";


echo "<br>";
$frog = new Frog("Buduk");
echo "Name : " . $frog->name ."<br>";
echo "Legs : " . $frog->legs ."<br>"; 
echo "cold blooded: " . $frog->cold_blooded ."<br>"; 
echo "Jump: ";
$kodok = new Frog("buduk");
echo $kodok->jump() ; 




?>